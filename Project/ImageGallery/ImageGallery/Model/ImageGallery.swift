//
//  ImageGallery.swift
//  ImageGallery
//
//  Created by Федор Штытько on 6/17/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

struct ImageGallery: Codable {
    var images = [ImageModel]()
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init?(json: Data) {
        guard let newvalue = try? JSONDecoder().decode(ImageGallery.self, from: json) else { return nil }
        self = newvalue
    }
    
    init() {
        self.images = [ImageModel]()
    }
    
}
