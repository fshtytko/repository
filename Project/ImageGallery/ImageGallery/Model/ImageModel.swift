//
//  ImageModel.swift
//  ImageGallery
//
//  Created by Федор Штытько on 6/17/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

struct ImageModel: Codable {
    var aspectRatio: Double
    var url: URL
}
