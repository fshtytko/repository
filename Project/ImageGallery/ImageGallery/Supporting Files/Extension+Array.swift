//
//  Extension+Array.swift
//  ImageGallery
//
//  Created by Федор Штытько on 6/21/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

extension Array {
    mutating func mutateEach( _ body: (inout Element) -> Void) {
        for index in self.indices {
            body(&self[index])
        }
    }
}
