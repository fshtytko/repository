//
//  AppDelegate.swift
//  ImageGallery
//
//  Created by Федор Штытько on 6/17/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func application(_ app: UIApplication, open inputURL: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        guard inputURL.isFileURL else { return false }
        
        // Reveal / import the document at the URL
        guard let documentBrowserViewController =
            window?.rootViewController as? DocumentBrowserViewController else { return false }
        
        documentBrowserViewController.revealDocument(at: inputURL,
                                                     importIfNeeded: true) { (revealedDocumentURL, error) in
                                                        if let error = error {
                                                            // Handle the error appropriately
                                                            print("Failed to reveal the document at URL \(inputURL) with error: '\(error)'")
                                                            return
                                                        }
                                                        
                                                        // Present the Document View Controller for the revealed URL
                                                        documentBrowserViewController.presentDocument(at: revealedDocumentURL!)
        }
        
        return true
    }
}
