//
//  ImageGalleryCollectionViewController.swift
//  ImageGallery
//
//  Created by Федор Штытько on 6/17/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//
//

import UIKit

class ImageGalleryCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout,
UICollectionViewDropDelegate, UICollectionViewDragDelegate {
    
    // MARK: - Public API, Model
    
    var imageGallery = ImageGallery()
    var document: ImageGalleryDocument?
    @IBOutlet var imageCollectionView: UICollectionView!
    
    // MARK: - Live cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.dropDelegate = self
        collectionView!.dragDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        document?.open(completionHandler: { _ in
            self.title = self.document?.localizedName
            self.imageGallery = self.document?.imageGallery ?? ImageGallery()
            self.imageGallery.images.mutateEach({ image in
                image.url.changeLocalURL()
            })
            self.collectionView?.reloadData()
        })
    }
    
    // MARK: - Work with Document
    
    @IBAction func closeDocument(_ sender: UIBarButtonItem) {
        documentChanged()
        document?.close()
        dismiss(animated: true)
    }
    
    private func documentChanged() {
        document?.imageGallery = imageGallery
        guard document?.imageGallery != nil else { return }
        document?.updateChangeCount(.done)
    }
    
    // MARK: - UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return imageGallery.images.count
    }
    
    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
        ) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ImageCollectionViewCell.cellIdentifier,
            for: indexPath)
        
        guard let imageCell = cell as? ImageCollectionViewCell else { return cell }
        imageCell.imageURL = imageGallery.images[indexPath.item].url
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let aspectRation = CGFloat(imageGallery.images[indexPath.item].aspectRatio)
        let width: CGFloat = 200.0
        return CGSize(width: width, height: width / aspectRation)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier, identifier == "Show Image",
            let imageCell = sender as? ImageCollectionViewCell, let indexPath = collectionView?.indexPath(for: imageCell),
            let imgvc = segue.destination as? ImageViewController else { return }
        imgvc.imageURL = imageGallery.images[indexPath.item].url
        
    }
    
    // MARK: - UICollectionViewDragDelegate
    
    func collectionView(_ collectionView: UICollectionView,
                        itemsForBeginning session: UIDragSession,
                        at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        itemsForAddingTo session: UIDragSession,
                        at indexPath: IndexPath,
                        point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard let itemCell = collectionView?.cellForItem(at: indexPath) as? ImageCollectionViewCell,
            let image = itemCell.imageGallery.image else { return [] }
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
        dragItem.localObject = indexPath
        return [dragItem]
    }
    
    // MARK: - UICollectionViewDropDelegate
    
    func collectionView(_ collectionView: UICollectionView,
                        canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext as?
            UICollectionView) == collectionView
        if isSelf {
            return session.canLoadObjects(ofClass: UIImage.self)
        } else {
            return session.canLoadObjects(ofClass: NSURL.self) &&
                session.canLoadObjects(ofClass: UIImage.self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?
        ) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy,
                                            intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ??
            IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath { // Drag locally
                collectionView.performBatchUpdates({
                    let draggedImage = imageGallery.images.remove(at: sourceIndexPath.item)
                    imageGallery.images.insert(draggedImage, at: destinationIndexPath.item)
                    imageCollectionView.deleteItems(at: [sourceIndexPath])
                    imageCollectionView.insertItems(at: [destinationIndexPath])
                })
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
                self.collectionView?.reloadData()
                self.documentChanged()
            } else {
                downloadImage(coordinator: item, at: destinationIndexPath.item)
                self.collectionView?.reloadData()
                self.documentChanged()
            }
        }
    }
    
    private func downloadImage(coordinator: UICollectionViewDropItem, at index: Int) {
        var aspectRatioLocal: Double?
        _ = coordinator.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { (provider, _) in
            DispatchQueue.main.async {
                guard let image = provider as? UIImage else { return }
                aspectRatioLocal = Double(image.size.width) /
                    Double(image.size.height)
            }
        }
        _ = coordinator.dragItem.itemProvider.loadObject(ofClass: URL.self, completionHandler: { (provider, _) in
            guard let url = provider?.imageURL else { return }
            self.imageGallery.images.insert(ImageModel(aspectRatio: aspectRatioLocal ?? 1.0, url: url), at: index)
            DispatchQueue.main.async {
                self.imageCollectionView.performBatchUpdates({
                    self.imageCollectionView.insertItems(at: [IndexPath(item: index, section: 0)])
                }, completion: nil)
            }
        })
    }
}
