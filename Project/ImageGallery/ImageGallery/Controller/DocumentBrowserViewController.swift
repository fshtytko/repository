//
//  DocumentBrowserViewController.swift
//  ImageGallery
//
//  Created by Федор Штытько on 6/17/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class DocumentBrowserViewController: UIDocumentBrowserViewController, UIDocumentBrowserViewControllerDelegate {
    
    var patternDocument: URL?
    let mainStoryBoard = "Main"
    let documentStoryBoard = "DocumentVC"
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        allowsDocumentCreation = true
        allowsPickingMultipleItems = false
        
        patternDocument = try? FileManager.default.url(for: .applicationSupportDirectory,
                                                       in: .userDomainMask,
                                                       appropriateFor: nil,
                                                       create: true).appendingPathComponent("Untitled.json")
        guard let pattern = patternDocument else { return }
        allowsDocumentCreation = FileManager.default.createFile(atPath: pattern.path, contents: Data())
    }
    
    // MARK: UIDocumentBrowserViewControllerDelegate
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController,
                         didRequestDocumentCreationWithHandler importHandler: @escaping (URL?,
        UIDocumentBrowserViewController.ImportMode) -> Void) {
        importHandler(patternDocument, .copy)
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        guard let sourceURL = documentURLs.first else { return }
        presentDocument(at: sourceURL)
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController,
                         didImportDocumentAt sourceURL: URL,
                         toDestinationURL destinationURL: URL) {
        presentDocument(at: destinationURL)
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController,
                         failedToImportDocumentAt documentURL: URL,
                         error: Error?) {
    }
    
    // MARK: Document Presentation
    
    func presentDocument(at documentURL: URL) {
        let storyBoard = UIStoryboard(name: mainStoryBoard, bundle: nil)
        let documentVC = storyBoard.instantiateViewController(withIdentifier: documentStoryBoard)
        guard let imageGalleryCollectionViewController = documentVC.contents
            as? ImageGalleryCollectionViewController else { return }
        imageGalleryCollectionViewController.document = ImageGalleryDocument(fileURL: documentURL)
        present(documentVC, animated: true)
    }
}
