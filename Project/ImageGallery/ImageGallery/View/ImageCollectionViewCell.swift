//
//  ImageCollectionViewCell.swift
//  ImageGallery
//
//  Created by Tatiana Kornilova on 20/06/2018.
//  Copyright © 2018 Stanford University. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageGallery: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Public API
    
    var imageURL: URL? {
        didSet {
            guard let url = imageURL else { return }
            updateUI(withURL: url)
        }
    }
    static let cellIdentifier = "Image Cell"
    let cache = URLCache.shared
    
    // MARK: - Private function
    
    private func updateUI(withURL url: URL) {
        let request = URLRequest(url: url)
        self.spinner.startAnimating()
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = self.cache.cachedResponse(for: request)?.data,
                let image = UIImage(data: data) else { return self.cache(request: request, url: url) }
            DispatchQueue.main.async {
                self.imageGallery?.image = image
                self.spinner?.stopAnimating()
            }
        }
    }
    
    private func cache(request: URLRequest, url: URL) {
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, _) in
            guard let data = data, let response = response,
                ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300,
                let image = UIImage(data: data), url == self.imageURL else { return }
            let cacheData = CachedURLResponse(response: response, data: data)
            self.cache.storeCachedResponse(cacheData, for: request)
            DispatchQueue.main.async {
                self.imageGallery?.image = image
                self.spinner?.stopAnimating()
            }
        }).resume()
    }
}
