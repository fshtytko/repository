//
//  Theme.swift
//  Concetration
//
//  Created by Федор Штытько on 4/19/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

struct ThemeConcetration {
    var name: String
    var emojis: [String]
}
