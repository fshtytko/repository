//
//  ModelConcetration.swift
//  Concetration
//
//  Created by Федор Штытько on 4/19/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

class Concentration {
    
    var cards = [CardConcetration]()
    var indexOfAndOneOnlyFaceUpCard: Int?
    var score = 0
    let winPoints = 2
    let losePoints = 1
    let selectedCard = 2
    var flipCount = 0
    
    init(numberOfPairsards: Int) {
        for _ in 1...numberOfPairsards {
            let card = CardConcetration()
            cards += [card, card]
        }
        cards.shuffle()
    }
    
    func newGame() {
        score = 0
        flipCount = 0
        for index in cards.indices {
            flipCards(flip: false, this: index)
            cards[index].isMatched = false
        }
        cards.shuffle()
    }
    
    func flipCards(flip: Bool, this index: Int) {
        cards[index].isFaceUP = flip
        cards[index].isSelected = flip
    }
    
    func chooseCard(at index: Int) {
        guard !cards[index].isMatched else {return}
        flipCount += 1
        scoreNumber(index)
        cards[index].isFaceUP = true
    }
    
    func scoreNumber(_ index: Int) {
        guard let mathcIndex = indexOfAndOneOnlyFaceUpCard else {
            flipChooseCards(index)
            return
        }
        guard mathcIndex != index else {return}
        scoreCount(at: mathcIndex, with: index)
    }
    
    //Score count
    func scoreCount(at mathcIndex: Int, with index: Int) {
        if cards[mathcIndex].identifier == cards[index].identifier {
            cards[mathcIndex].isMatched = true
            cards[index].isMatched = true
            //good cards choose
            score += winPoints
        } else {
            //bad cards choose
            if cards[index].isSelected && cards[mathcIndex].isSelected {
                score -= selectedCard*losePoints
            } else if cards[index].isSelected || cards[mathcIndex].isSelected {
                score -= losePoints
            }
        }
        flipCards(flip: true, this: index)
        cards[mathcIndex].isSelected = true
        indexOfAndOneOnlyFaceUpCard = nil
    }
    
    //Flip Card
    func flipChooseCards(_ index: Int) {
        for flipdownIndex in cards.indices {
            cards[flipdownIndex].isFaceUP = false
        }
        indexOfAndOneOnlyFaceUpCard = index
    }
}
