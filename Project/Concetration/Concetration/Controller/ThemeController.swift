//
//  ThemeControllerViewController.swift
//  Concetration
//
//  Created by Федор Штытько on 5/23/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class ThemeController: UIViewController {
    
    var indexTheme = 0
    
    @IBAction func themeButtom(_ sender: UIButton) {
        guard let title = sender.titleLabel?.text else {
            print("Error")
            return
        }
        switch title {
        case "Animals":
            indexTheme = 0
        case "Fishs":
            indexTheme = 1
        case "Green":
            indexTheme = 2
        case "Flowers":
            indexTheme = 3
        case "Fruits":
            indexTheme = 4
        case "Food":
            indexTheme = 5
        default:
            break
        }
        showConcetration()
    }
    
    // MARK: - Navigation
    
    func showConcetration() {
        let concetration = storyboard?.instantiateViewController(withIdentifier: "Concetration")
            as? ConcetrationController
        concetration!.indexTheme = indexTheme
        self.navigationController?.pushViewController(concetration!, animated: true)
    }
}
