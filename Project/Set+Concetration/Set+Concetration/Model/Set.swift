//
//  Set.swift
//  Set
//
//  Created by Федор Штытько on 4/24/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

class Set {
    var cards = [CardSet]()
    var cardsOnTable = [CardSet]()
    var cardsSelected = [CardSet]()
    var cardsTrySet = [CardSet]()
    var isMatched: Bool? {
        get {
            return checkSetCard(cards: cardsTrySet)
        }
    }
    var isSet: Bool? {
        get {
            guard cardsTrySet.count == 3 else { return nil }
            return checkSetCard(cards: cardsTrySet)
        }
        set {
            cardsTrySet = cardsSelected
            cardsSelected.removeAll()
        }
    }
    var hints: [[Int]] {
        guard cardsOnTable.count > 2 else { return self.hints }
        return getAmountSet()
    }
    var allVariantPropiertise: [Variant] { return [.variantOne, .variantTwo, .variantThree] }
    let deckCards = 12
    
    init() {
        for symbol in allVariantPropiertise {
            for color in allVariantPropiertise {
                for texture in allVariantPropiertise {
                    for amount in allVariantPropiertise {
                        cards.append(CardSet(symbol: symbol, color: color, texture: texture, amont: amount))
                    }
                }
            }
        }
        cards.shuffle()
    }
    
    //first 12 cards on table
    func getCardsOnTable() {
        for index in 0..<deckCards {
            let cardChoosen = cards.remove(at: index)
            cardsOnTable.append(cardChoosen)
        }
    }
    
    // array from 3 element for reuse
    func getThreeCards() {
        guard 3 < cards.count else { return }
        for indexLastCard in 0..<3 {
            let threeCards = cards.remove(at: indexLastCard)
            cardsOnTable.append(threeCards)
        }
    }
    
    //set Check
    func checkSetCard(cards: [CardSet]) -> Bool {
        guard cards.count == 3 else { return false }
        let sum = [
            cards.reduce(0, { $0 + $1.symbol.rawValue }),
            cards.reduce(0, { $0 + $1.amont.rawValue }),
            cards.reduce(0, { $0 + $1.texture.rawValue }),
            cards.reduce(0, { $0 + $1.color.rawValue })
        ]
        return sum.reduce(true, { $0 && ($1 % 3 == 0) })
    }
    
    // delete or replace element after  setCheck
    func removeOrReplaceCard() {
        guard 3 < cards.count else {
            cardsOnTable.remove(elements: cardsTrySet)
            cardsTrySet.removeAll()
            return
        }
        cardsOnTable.remove(elements: cardsTrySet)
        getThreeCards()
        cardsTrySet.removeAll()
    }
    
    func chooseCard(at index: Int) {
        let cardChoosen = cardsOnTable[index]
        guard  !cardsTrySet.contains(cardChoosen) else { return }
        if  isSet != nil {
            guard isSet! else { return isSet = nil }
            removeOrReplaceCard()
        }
        guard cardsSelected.count == 2, !cardsSelected.contains(cardChoosen)
            else { return cardsSelected.inOut(element: cardChoosen) }
        cardsSelected.append(cardChoosen)
        isSet = checkSetCard(cards: cardsSelected)
    }
    
    //give 3 elements and check for set. look all table
    func getAmountSet() -> [[Int]] {
        var hints = [[Int]]()
        for firstCard in 0..<cardsOnTable.count {
            for secondCard in (firstCard + 1)..<cardsOnTable.count {
                for thirdCard in (secondCard + 1)..<cardsOnTable.count {
                    let cards = [cardsOnTable[firstCard], cardsOnTable[secondCard], cardsOnTable[thirdCard]]
                    if checkSetCard(cards: cards) {
                        hints.append([firstCard, secondCard, thirdCard])
                    }
                }
            }
        }
        return hints
    }
}
