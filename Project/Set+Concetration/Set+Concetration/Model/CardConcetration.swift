//
//  Card.swift
//  Concetration
//
//  Created by Федор Штытько on 4/22/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

struct CardConcetration {
    var isFaceUP = false
    var isMatched = false
    var isSelected = false
    var identifier: Int
    static var identifierFactory = 0
    
    init() {
        self.identifier = CardConcetration.getUniqueIdentifier()
    }
    
    //get identifier
    static func getUniqueIdentifier() -> Int {
        CardConcetration.identifierFactory += 1
        return CardConcetration.identifierFactory
    }
}
