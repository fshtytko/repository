//
//  ViewController.swift
//  Concetration
//
//  Created by Федор Штытько on 4/18/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class ConcetrationController: UIViewController {
    
    @IBOutlet var cardButtons: [UIButton]!
    
    var pullTheme: [ThemeConcetration] = [
        ThemeConcetration(name: "Animals",
                          emojis: ["🐒", "🐷", "🐻", "🐶", "🐱", "🐭", "🦊", "🐼"]),
        ThemeConcetration(name: "Fishs",
                          emojis: ["🐟", "🐬", "🐠", "🐳", "🐋", "🦈", "🐡", "🐙"]),
        ThemeConcetration(name: "Green",
                          emojis: ["🌴", "🌳", "🌲", "🌱", "🌿", "☘️", "🌵", "🍃"]),
        ThemeConcetration(name: "Flowers",
                          emojis: ["🌸", "🌼", "🌹", "🥀", "🌻", "🌷", "💐", "🌾"]),
        ThemeConcetration(name: "Fruits",
                          emojis: ["🍏", "🍎", "🍊", "🍐", "🍋", "🍌", "🍇", "🍍"]),
        ThemeConcetration(name: "Food",
                          emojis: ["🥪", "🍟", "🌭", "🍔", "🌮", "🍕", "🍗", "🥐"])
    ]
    
    var emojiChoise = [String]()
    var emoji = [Int: String]()
    var indexTheme = 0 {
        didSet {
            emojiChoise = pullTheme[indexTheme].emojis
            emoji = [Int: String]()
        }
    }
    lazy var game = Concentration(numberOfPairsards: (cardButtons.count + 1) / 2)
    @IBOutlet weak var scoreCountLabel: UILabel!
    @IBOutlet weak var flipCountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewFromModel()
    }
    
    //New game
    @IBAction func newGameButton() {
        indexTheme = Int.random(in: 0..<pullTheme.count)
        game.newGame()
        updateViewFromModel()
    }
    
    //Flip cards
    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else {return}
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUP {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
        scoreCountLabel.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flipCount)"
    }
    
    //Choose emoji
    func emoji(for card: CardConcetration) -> String {
        if emoji[card.identifier] == nil, emojiChoise.count > 0 {
            let randomIndex = Int.random(in: 0..<emojiChoise.count)
            emoji[card.identifier] = emojiChoise.remove(at: randomIndex)
        }
        print(emoji)
        return emoji[card.identifier] ?? "?"
    }
}
