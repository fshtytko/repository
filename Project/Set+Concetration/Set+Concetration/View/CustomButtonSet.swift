//
//  CardView.swift
//  CardView
//
//  Created by Федор Штытько on 5/14/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class CustomButtonSet: UIButton {
    
    enum Amount: Int {
        case one
        case two
        case three
    }
    
    enum Symbol: Int {
        case diamond
        case squiggle
        case oval
    }
    
    enum Fill: Int {
        case empty
        case stripes
        case full
    }
    
    enum Colors: Int {
        case orange
        case blue
        case green
    }
    
    var set: CardSet? { didSet { getAttribute() } }
    var amount = Amount.one {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    var isMatched: Bool? {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var isFaceUp = false {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    var color = Colors.green {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    var fill = Fill.stripes {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    var symbol = Symbol.oval {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    override func draw(_ rect: CGRect) {
        if isFaceUp {
            getAttribute()
            layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            layer.backgroundColor = #colorLiteral(red: 0.9402636886, green: 0.4986512065, blue: 0.3506743312, alpha: 1)
        }
    }
    
    func getAttribute() {
        let size = CGSize(width: bounds.maxX * 0.8, height: bounds.maxY * 0.25)
        let centreFrame = bounds.midY - size.height / 2
        var origin = CGPoint(x: bounds.maxX * 0.10, y: centreFrame)
        var rect = CGRect(origin: origin, size: size)
        switch amount {
        case .one:
            drawSymbol(in: rect)
        case .two:
            origin = CGPoint(x: bounds.maxX * 0.10, y: centreFrame - size.height * 0.6)
            rect = CGRect(origin: origin, size: size)
            drawSymbol(in: rect)
            origin = CGPoint(x: bounds.maxX * 0.10, y: centreFrame + size.height * 0.6)
            rect = CGRect(origin: origin, size: size)
            drawSymbol(in: rect)
        case .three:
            drawSymbol(in: rect)
            origin = CGPoint(x: bounds.maxX * 0.10, y: centreFrame + size.height * 1.1)
            rect = CGRect(origin: origin, size: size)
            drawSymbol(in: rect)
            origin = CGPoint(x: bounds.maxX * 0.10, y: centreFrame - size.height * 1.1)
            rect = CGRect(origin: origin, size: size)
            drawSymbol(in: rect)
        }
    }
    
    func getBorderAndBackground(color: CGColor) {
        self.layer.borderColor = color
        self.layer.borderWidth = 3.0
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        guard color == #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1) else { return isMatched = false }
        isMatched = true
    }
    
    private func drawSymbol(in rect: CGRect) {
        var drawingSymbol = UIBezierPath()
        switch symbol {
        case .diamond:
            drawingSymbol = pathForDiamond(in: rect)
        case .squiggle:
            drawingSymbol = pathForSquiggle(in: rect)
        case .oval:
            drawingSymbol = pathForOval(in: rect)
        }
        getColorSymbol()
        drawingSymbol.stroke()
        getFill(drawingSymbol, rect: rect)
        
    }
    
    func getColorSymbol () {
        let symbolColor: UIColor
        switch color {
        case .blue:
            symbolColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        case .green:
            symbolColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case .orange:
            symbolColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
        }
        symbolColor.setFill()
        symbolColor.setStroke()
    }
    
    func getFill(_ drawingSymbol: UIBezierPath, rect: CGRect) {
        switch fill {
        case .stripes:
            let context = UIGraphicsGetCurrentContext()
            context?.saveGState()
            drawingSymbol.addClip()
            stripingSymbol(in: rect, symbol: drawingSymbol)
            context?.restoreGState()
        case .full:
            drawingSymbol.fill()
        default:
            break
        }
    }
    
    //stripes any symbol
    private func stripingSymbol(in rect: CGRect, symbol: UIBezierPath) {
        let spaceInStripe = bounds.maxX * 0.04
        let longFrame = Int(bounds.maxX / spaceInStripe)
        var placeX = bounds.minX
        for _ in 1...longFrame {
            symbol.move(to: CGPoint(x: placeX, y: bounds.maxY))
            symbol.addLine(to: CGPoint(x: placeX, y: bounds.minY))
            placeX += spaceInStripe
            symbol.stroke()
        }
        symbol.stroke()
    }
    
    //draw Squiggle
    private func pathForSquiggle(in rect: CGRect) -> UIBezierPath {
        let upperSquiggle = UIBezierPath()
        let sqdx = rect.width * 0.1
        let sqdy = rect.height * 0.2
        upperSquiggle.move(to: CGPoint(x: rect.minX, y: rect.midY))
        upperSquiggle.addCurve(to:
            CGPoint(x: rect.minX + rect.width * 1/2,
                    y: rect.minY + rect.height / 8),
                               controlPoint1: CGPoint(x: rect.minX,
                                                      y: rect.minY),
                               controlPoint2: CGPoint(x: rect.minX + rect.width * 1/2 - sqdx,
                                                      y: rect.minY + rect.height / 8 - sqdy))
        upperSquiggle.addCurve(to:
            CGPoint(x: rect.minX + rect.width * 4/5,
                    y: rect.minY + rect.height / 8),
                               controlPoint1: CGPoint(x: rect.minX + rect.width * 1/2 + sqdx,
                                                      y: rect.minY + rect.height / 8 + sqdy),
                               controlPoint2: CGPoint(x: rect.minX + rect.width * 4/5 - sqdx,
                                                      y: rect.minY + rect.height / 8 + sqdy))
        upperSquiggle.addCurve(to:
            CGPoint(x: rect.minX + rect.width,
                    y: rect.minY + rect.height / 2),
                               controlPoint1: CGPoint(x: rect.minX + rect.width * 4/5 + sqdx,
                                                      y: rect.minY + rect.height / 8 - sqdy ),
                               controlPoint2: CGPoint(x: rect.minX + rect.width,
                                                      y: rect.minY))
        let lowerSquiggle = UIBezierPath(cgPath: upperSquiggle.cgPath)
        lowerSquiggle.apply(CGAffineTransform.identity.rotated(by: CGFloat.pi))
        lowerSquiggle.apply(CGAffineTransform.identity.translatedBy(
            x: bounds.width,
            y: bounds.height))
        upperSquiggle.move(to: CGPoint(x: rect.minX, y: rect.midY))
        upperSquiggle.append(lowerSquiggle)
        return upperSquiggle
    }
    
    private func pathForOval(in rect: CGRect) -> UIBezierPath {
        let oval = UIBezierPath()
        let radius = rect.height / 2
        oval.addArc(withCenter: CGPoint(x: rect.minX + radius,
                                        y: rect.minY + radius),
                    radius: radius,
                    startAngle: CGFloat.pi/2,
                    endAngle: CGFloat.pi*3/2,
                    clockwise: true)
        oval.addLine(to: CGPoint(x: rect.maxX - radius,
                                 y: rect.minY))
        oval.addArc(withCenter: CGPoint(x: rect.maxX - radius,
                                        y: rect.maxY - radius),
                    radius: radius,
                    startAngle: CGFloat.pi*3/2,
                    endAngle: CGFloat.pi/2,
                    clockwise: true)
        oval.close()
        return oval
    }
    
    private func pathForDiamond(in rect: CGRect) -> UIBezierPath {
        let diamond = UIBezierPath()
        diamond.move(to: CGPoint(x: rect.minX, y: rect.midY))
        diamond.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        diamond.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        diamond.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        diamond.close()
        return diamond
    }
    
    //animations for one cards
    func animateDeal(from deckCenter: CGPoint, delay: TimeInterval) {
        let currentCenter = center
        let currentBounds = bounds
        isFaceUp = false
        center =  deckCenter
        alpha = 1
        bounds = CGRect(x: 0.0, y: 0.0, width: 0.3 * bounds.width,
                        height: 0.3 * bounds.height)
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = currentCenter
                self.bounds = currentBounds
        },
            completion: { _ in UIView.transition(
                with: self,
                duration: 0.75,
                options: [.transitionFlipFromRight],
                animations: {
                    self.isFaceUp = !self.isFaceUp
            })
        })
    }
    
    func copy(with zone: NSZone? = nil) -> CustomButtonSet {
        let copy = CustomButtonSet()
        copy.amount = amount
        copy.color = color
        copy.symbol = symbol
        copy.fill = fill
        copy.isFaceUp = true
        copy.bounds = bounds
        copy.frame = frame
        copy.alpha = 1
        return copy
    }
    
    func animateFly(to discardPileCenter: CGPoint, delay: TimeInterval) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = discardPileCenter
        })
        animatedFlipWhenFly()
    }
    
    func animatedFlipWhenFly() {
        UIView.transition(
            with: self,
            duration: 0.75,
            options: [.transitionFlipFromLeft],
            animations: {
                self.isFaceUp = false
                self.transform = CGAffineTransform.identity
                    .rotated(by: CGFloat.pi / 2.0)
                self.bounds = CGRect(x: 0.0, y: 0.0,
                                     width: 0.4 * self.bounds.width,
                                     height: 0.4 * self.bounds.height)
        })
    }
}
