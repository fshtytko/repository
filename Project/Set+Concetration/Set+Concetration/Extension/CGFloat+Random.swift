//
//  CGFloat+.swift
//  Set
//
//  Created by Федор Штытько on 6/2/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//
import UIKit

extension CGFloat {
    var random: CGFloat {
        return self * CGFloat(UInt32.random(in: .min ... .max) / UInt32.max)
    }
}
