//
//  Array+.swift
//  Set
//
//  Created by Федор Штытько on 5/13/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    // in or out Cards for click
    mutating func inOut(element: Element) {
        guard let from = self.firstIndex(of: element) else {
            return self.append(element)
        }
        self.remove(at: from)
    }
    
    // delete array element
    mutating func remove(elements: [Element]) {
        self = self.filter { !elements.contains($0) }
    }
}
