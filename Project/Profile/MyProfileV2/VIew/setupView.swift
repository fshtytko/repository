//
//  mainImageView.swift
//  MyProfileV2
//
//  Created by Федор Штытько on 4/10/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class setupView:UIImageView {
    
    func roundImage (_ mainImageView: UIImageView){
        mainImageView.layer.cornerRadius = mainImageView.frame.size.height/2
        mainImageView.layer.borderWidth = 1
        mainImageView.clipsToBounds = true
    }
    
    func setupLabel (_ namelabel: UILabel, _ text: String){
        namelabel.text = text
    }
}
