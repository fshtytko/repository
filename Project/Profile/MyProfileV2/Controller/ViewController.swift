//
//  ViewController.swift
//  MyProfileV2
//
//  Created by Федор Штытько on 4/10/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var jobNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let objMainImageView = setupView()
        objMainImageView.roundImage(mainImageView)
        objMainImageView.setupLabel(fullNameLabel, "Федор Штытько")
        objMainImageView.setupLabel(jobNameLabel, "Стажер")
    }
}

