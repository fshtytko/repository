//
//  SecondViewController.swift
//  MyProfileV2
//
//  Created by Федор Штытько on 4/17/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import SnapKit

class SecondViewController: UIViewController {
    let fullNameLabel = UILabel()
    let jobNameLabel = UILabel()
    var mainImageView = UIImageView()
    let personImage = UIImage(named: "homa@2x")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: -Setup label
        self.view.addSubview(self.fullNameLabel)
        self.view.addSubview(self.jobNameLabel)
        let labelFont = UIFont.boldSystemFont(ofSize: 27)
        fullNameLabel.text = "Федор Штытько"
        jobNameLabel.text = "Стажер"
        fullNameLabel.font = labelFont
        jobNameLabel.font = labelFont
  
        //MARK: -Image
        mainImageView = UIImageView(image: personImage)
        self.view.addSubview(self.mainImageView)
        mainImageView.layer.cornerRadius = mainImageView.frame.size.height/2
        mainImageView.clipsToBounds = true
        mainImageView.layer.borderWidth = 1
        
        //MARK: -Setup Constraints
        setupFullNameLabel()
        setupJobNameLabel()
        setupConstraints()
        }
    
        //MARK: -Func Constraints Setup
        func setupConstraints(){
            mainImageView.snp.makeConstraints{(make) -> Void in
            make.width.equalTo(mainImageView.snp.height)
            make.center.equalTo(self.view)
            }
        }
    
        func setupFullNameLabel(){
            fullNameLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(mainImageView.snp_bottom).offset(30.5)
            make.centerX.equalTo(self.view)
            }
        }
    
        func setupJobNameLabel(){
            jobNameLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(fullNameLabel.snp_bottom).offset(5)
            make.centerX.equalTo(self.view)
            }
        }
    
}
