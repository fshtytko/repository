//
//  Card.swift
//  Set
//
//  Created by Федор Штытько on 4/24/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

struct CardSet: Equatable {
    let symbol: Variant  // - symbol - cirlce, tungle, square
    let color: Variant  // - color -  blue , green, orange
    let texture: Variant // - texture - nil, штрихованный, полный
    let amont: Variant  // - amont - 1,2,3
}
