//
//  bordView.swift
//  Set
//
//  Created by Федор Штытько on 5/15/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class BordViewSet: UIView {
    
    struct Constant {
        static let cellAspectRatio: CGFloat = 0.7
        static let spacingDx: CGFloat  = 3.0
        static let spacingDy: CGFloat  = 3.0
    }
    
    var cardViews = [CustomButtonSet]()
    var rowsGrid: Int { return grid?.dimensions.rowCount ?? 0 }
    var grid: Grid?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var grid = Grid(
            layout: .aspectRatio(Constant.cellAspectRatio),
            frame: bounds)
        grid.cellCount = cardViews.count
        getCardsView(grid)
    }
    
    func addSubviews(card: [CustomButtonSet]) {
        cardViews +=  card
        cardViews.forEach { (customButton) in
            addSubview(customButton)
        }
        layoutIfNeeded()
    }
    
    func resetSubviews() {
        cardViews.forEach { (cardView) in
            cardView.removeFromSuperview()
        }
        layoutIfNeeded()
    }
    
    private func getCardsView(_ grid: Grid) {
        for row in 0..<grid.dimensions.rowCount {
            for column in 0..<grid.dimensions.columnCount {
                guard cardViews.count > (row * grid.dimensions.columnCount + column) else { return }
                UIViewPropertyAnimator.runningPropertyAnimator(
                    withDuration: 0.4,
                    delay: TimeInterval(row) * 0.1,
                    options: [.curveEaseInOut],
                    animations: {
                        self.cardViews[row * grid.dimensions.columnCount + column].frame =
                            grid[row, column]!.insetBy(
                                dx: Constant.spacingDx,
                                dy: Constant.spacingDy)
                })
            }
        }
    }
}
