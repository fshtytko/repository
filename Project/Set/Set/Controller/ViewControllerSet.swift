//
//  ViewController.swift
//  Set
//
//  Created by Федор Штытько on 4/24/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import UIKit

class ViewControllerSet: UIViewController {
    
    @IBOutlet weak var startDeck: UIView!
    @IBOutlet weak var endDeck: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var boardView: BordViewSet! {
        didSet {
            let swipe = UISwipeGestureRecognizer(target: self, action: #selector(getThreeCardsButton))
            let rotaite = UIRotationGestureRecognizer(target: self, action: #selector(shuffle))
            swipe.direction = [.down]
            boardView.addGestureRecognizer(swipe)
            boardView.addGestureRecognizer(rotaite)
        }
    }
    private var matchedSetCardViews: [CustomButtonSet] {
        return  boardView.cardViews.filter { $0.isMatched == true }
    }
    private var tempCards = [CustomButtonSet]()
    
    // MARK: - Dynamic Animation
    lazy var animator = UIDynamicAnimator(referenceView: self.boardView)
    lazy var cardBehavior = CardBehavior(in: animator)
    
    private var centerEndDeck: CGPoint {
        return bottomView.convert(endDeck.center, to: boardView)
    }
    
    private var centerStartDeck: CGPoint {
        return bottomView.convert(startDeck.center, to: boardView)
    }
    
    private var newCards: [CustomButtonSet] {
        return boardView.cardViews.filter { $0.alpha == 0 }
    }
    @IBOutlet weak var cardsCountLabel: UILabel!
    @IBOutlet weak var setCountLabel: UILabel!
    var game = Set()
    var buttonCustom = CustomButtonSet()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        game.getCardsOnTable()
        updateViewModel()
    }
    
    @IBAction private func newGameButton() {
        game = Set()
        boardView.resetSubviews()
        boardView.cardViews = []
        game.getCardsOnTable()
        updateViewModel()
    }
    
    @IBAction private func getHints() {
        guard  game.hints.count > 0 else { return }
        game.hints[0].forEach { (index) in
            let button = boardView.cardViews[index]
            button.getBorderAndBackground(color: #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1))
        }
    }
    
    @IBAction private func getThreeCardsButton() {
        guard game.cardsOnTable.count < 81 else { return }
        game.getThreeCards()
        updateViewModel()
    }
    
    @objc private func shuffle(_ sender: UIRotationGestureRecognizer) {
        guard case .ended = sender.state else { return }
        newGameButton()
    }
    
    @objc private func chooseCard(_ sender: UITapGestureRecognizer) {
        guard case .ended = sender.state, let cardView = sender.view! as? CustomButtonSet else { return }
        game.chooseCard(at: boardView.cardViews.firstIndex(of: cardView)!)
        updateViewModel()
    }
    
    private func updateViewModel() {
        updateButtons()
        cardsCountLabel.text = "Cards: \(game.cards.count)"
        setCountLabel.text = " Set: \(game.hints.count)"
    }
    
    private func updateCardView(card: CardSet, buttonCustom: CustomButtonSet) {
        buttonCustom.amount = CustomButtonSet.Amount(rawValue: card.amont.rawValue) ?? .one
        buttonCustom.color = CustomButtonSet.Colors(rawValue: card.color.rawValue) ?? .orange
        buttonCustom.symbol = CustomButtonSet.Symbol(rawValue: card.symbol.rawValue) ?? .diamond
        buttonCustom.fill = CustomButtonSet.Fill(rawValue: card.texture.rawValue) ?? .empty
        buttonCustom.getBorderAndBackground(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0))
        if game.cardsSelected.contains(card) {
            buttonCustom.getBorderAndBackground(color: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
        } else {
            guard let isSet = game.isSet, game.cardsTrySet.contains(card) else { return }
            buttonCustom.getBorderAndBackground(color: isSet ? #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1) : #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
        }
    }
    
    private func getGesture(button: CustomButtonSet) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCard))
        button.addGestureRecognizer(tap)
    }
    
    private func updateButtons() {
        let amountCardsOnView =  boardView.cardViews.count
        for index in game.cardsOnTable.indices {
            let card = game.cardsOnTable[index]
            if index > (amountCardsOnView - 1) { //new cards
                let button = CustomButtonSet()
                updateCardView(card: card, buttonCustom: button)
                button.alpha = 0
                getGesture(button: button)
                boardView.addSubviews(card: [button])
            } else {    //old cards
                let button = boardView.cardViews [index]
                if button.alpha < 1 && button.alpha > 0 && game.isSet != true {
                    button.alpha = 0
                }
                updateCardView(card: card, buttonCustom: button)
            }
        }
        flyAwayAnimation()
        dealNewCardsAnimations()
    }
    
    // start animations deal for one card
    private func dealNewCardsAnimations() {
        var amountNewCard = 0
        let timeInterval =  0.15  * Double(boardView.rowsGrid + 1)
        Timer.scheduledTimer(withTimeInterval: timeInterval,
                             repeats: false) { (_) in
                                for cardView in self.newCards {
                                    cardView.animateDeal(from: self.centerStartDeck,
                                                         delay: TimeInterval(amountNewCard) * 0.25)
                                    amountNewCard += 1
                                }
        }
    }
    
    private func flyAwayAnimation () {
        
        // Matched cards animation
        let alreadyFliedCount = matchedSetCardViews.filter { ($0.alpha < 1 && $0.alpha > 0) }.count
        guard  game.isSet != nil, game.isSet!, alreadyFliedCount == 0 else { return }
        
        tempCards.forEach { tmpCard in
            tmpCard.removeFromSuperview()
        }
        tempCards = []
        
        matchedSetCardViews.forEach { cardView in
            cardView.alpha = 0.1
            tempCards.append(contentsOf: [cardView.copy()])
        }
        
        tempCards.forEach { tmpCard in
            boardView.addSubview(tmpCard)
            cardBehavior.addItem(tmpCard)
        }
        
        boardView.cardViews.forEach { cardView in
            cardView.isMatched = false
        }
        
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (_) in
            var time = 1
            for tempCard in self.tempCards {
                self.cardBehavior.removeItem(tempCard)
                tempCard.animateFly(to: self.centerEndDeck,
                                    delay: TimeInterval(time) * 0.25)
                time += 1
            }
        }
        
    }
}
