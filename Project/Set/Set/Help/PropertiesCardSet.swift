//
//  PropertiesCard.swift
//  Set
//
//  Created by Федор Штытько on 4/25/19.
//  Copyright © 2019 Федор Штытько. All rights reserved.
//

import Foundation

enum Variant: Int {
    case variantOne = 1// - cirlce, blue, nil, 1
    case variantTwo = 2 // - tungle, green, hatched, 2
    case variantThree  = 3 // - square, orange, full, 3
    
    var variantIndex: Int { return (self.rawValue - 1) }
}
